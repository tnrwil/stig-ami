#Enabling repos and install certbot and strongswan

sudo yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional
sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm 
sudo rpm  --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
sudo yum install ansible python2-passlib ruby firewalld wget certbot strongswan -y 
systemctl enable strongswan
cd /tmp
sudo wget https://s3.amazonaws.com/ec2-downloads/ec2-ami-tools.noarch.rpm
sudo rpm -K ec2-ami-tools.noarch.rpm
sudo rpm -Kv ec2-ami-tools.noarch.rpm 
sudo yum install -y /tmp/ec2-ami-tools.noarch.rpm
export RUBYLIB=$RUBYLIB:/usr/lib/ruby/site_ruby:/usr/lib64/ruby/site_ruby
export PATH=$PATH:$RUBYLIB:/usr/local/bin/
ec2-ami-tools-version
ec2-bundle-vol --help

# Setting firewalld ports and reloading
sudo systemctl start firewalld
sudo firewall-cmd --zone=public --permanent --add-port=4500/tcp
sudo firewall-cmd --zone=public --permanent --add-port=443/tcp
sudo firewall-cmd --zone=public --permanent --add-port=22/tcp
sudo firewall-cmd --zone=public --permanent --add-port=500/tcp
sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule protocol value="esp" accept'
sudo firewall-cmd --zone=public --permanent --add-rich-rule='rule protocol value="ah" accept'
sudo firewall-cmd --zone=public --permanent --add-service="ipsec"
sudo firewall-cmd --zone=public --permanent --add-masquerade
sudo firewall-cmd --reload
sudo systemctl restart firewalld


#enable port forwarding
echo "net.ipv4.ip_forward=1" >> /etc/sysctl.d/99-sysctl.conf 
sudo echo "net.ipv4.conf.all.accept_redirects=0" >> /etc/sysctl.d/99-sysctl.conf 
sudo echo "net.ipv4.conf.all.send_redirects=0" >> /etc/sysctl.d/99-sysctl.conf 
sudo sysctl --system

#Start strongswan
sudo systemctl start strongswan
sudo systemctl enable strongswan
