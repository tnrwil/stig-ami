# StrongVpn

This is role will configure StrongSwan from the StrongSwan AMI created.  The ami has everything installed.
Here is the configuration left:

- Configuring ssl certs with certbot
- Placing the configuration files in the proper location with proper permissions
- Enabling services for strongswan
- Configuring ipsec.conf file
- Setting up authentication
- Configuring iptables
- Setting up portforwarding
- Testing connection


## Requirements

You will need the ami name is `strongswan-vpn`. Its is setup in us-east-1 region.

## Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

## Dependencies

Need to ensure the following rpms are installed ( taken care of by ami)
- certbot
- wget
- firewalld
- strongswan


## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: <somehostname>
      roles:
         - strongswan

## License

BSD

## Author Information
Tameika Reed
